package com.example.chister

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ingresarButton : Button = findViewById(R.id.ingresar_chiste)
        val calificarButton : Button = findViewById(R.id.calificar_chiste)
        val recomendarButton : Button = findViewById(R.id.recomendar_chiste)

        ingresarButton.setOnClickListener {
            val intent = Intent(this,IngresarChiste::class.java)
            startActivity(intent)
        }

        calificarButton.setOnClickListener {
            val intent = Intent(this,CargarChistes::class.java)
            startActivity(intent)
        }

        recomendarButton.setOnClickListener {
            val intent = Intent(this,CargarChistes::class.java)
            startActivity(intent)
        }
    }
}
