package com.example.chister

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class ChisteAdapter(val mContext : Context, val layoutResId: Int,val chisteList:List<Chiste>)
    : ArrayAdapter<Chiste>(mContext,layoutResId, chisteList) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater : LayoutInflater = LayoutInflater.from(mContext)
        val view : View =  layoutInflater.inflate(layoutResId,null)
        val textViewTitulo = view.findViewById<TextView>(R.id.textview_titulo)
        val textViewChiste = view.findViewById<TextView>(R.id.textview_chiste)
        val textViewRating = view.findViewById<TextView>(R.id.textview_rating)
        val chiste = chisteList[position]


        textViewTitulo.text = chiste.titulo
        textViewChiste.text = chiste.texto
        textViewRating.text = if(chiste.puntaje!=0.toFloat()) chiste.puntaje.toString()+"\u272f" else ""

        //view.setOnClickListener {
        //    Log.i("Jhalep","texto del chiste: "+chiste.texto)
        //}

        return view
    }



}