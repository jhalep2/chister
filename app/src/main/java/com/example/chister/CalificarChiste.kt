package com.example.chister

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RatingBar
import android.widget.TextView
import com.google.firebase.database.*

class CalificarChiste : AppCompatActivity() {

    lateinit var mFirebaseDatabase: FirebaseDatabase;
    lateinit var referenciaBD: DatabaseReference;

    lateinit var textViewTitulo : TextView
    lateinit var textViewTexto : TextView
    lateinit var textViewPersonajes : TextView
    lateinit var calificarButton : Button
    lateinit var ratingBar: RatingBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calificar_chiste)

        textViewTitulo = findViewById(R.id.textview_titulo_full)
        textViewTexto = findViewById(R.id.textview_chiste_full)
        textViewPersonajes = findViewById(R.id.textview_chiste)
        calificarButton = findViewById(R.id.calificar_button)
        ratingBar = findViewById(R.id.ratingBar)

        val chisteId = intent.getStringExtra("chisteId")

        mFirebaseDatabase = FirebaseDatabase.getInstance()
        referenciaBD = mFirebaseDatabase.getReference().child("chistes").child(chisteId)

        referenciaBD.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()){
                    val chiste = p0.getValue(Chiste::class.java)
                    textViewTitulo.text = chiste!!.titulo
                    textViewTexto.text = chiste!!.texto
                    textViewPersonajes.text = chiste!!.personajes
                    ratingBar.rating = chiste!!.puntaje

                    calificarButton.setOnClickListener {
                        chiste.puntaje = ratingBar.rating
                        referenciaBD.setValue(chiste).addOnCompleteListener{
                            finish()
                        }
                    }
                }



            }
        })

    }
}
