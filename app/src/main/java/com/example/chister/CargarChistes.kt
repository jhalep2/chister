package com.example.chister

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import com.google.firebase.database.*

class CargarChistes : AppCompatActivity() {

    lateinit var mFirebaseDatabase: FirebaseDatabase;
    lateinit var referenciaBD: DatabaseReference;

    lateinit var chisteList : MutableList<Chiste>
    lateinit var listView : ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cargar_chistes)

        listView = findViewById(R.id.list_view)

        chisteList = mutableListOf()
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        referenciaBD = mFirebaseDatabase.reference.child("chistes")

        referenciaBD.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()){
                    chisteList.clear()
                    for (c in p0.children){
                        val chiste = c.getValue(Chiste::class.java)
                        chisteList.add(chiste!!)
                    }

                    val adapter = ChisteAdapter(applicationContext,R.layout.chiste_item, chisteList)
                    listView.adapter = adapter
                    listView.setOnItemClickListener { _, _, position, _ ->
                        //Log.i("Jhalep","elemento:"+chisteList[position].chisteId)
                        val intent = Intent(applicationContext,CalificarChiste::class.java)
                        intent.putExtra("chisteId",chisteList[position].chisteId)
                        startActivity(intent)
                    }
                }
            }

        })
    }
}
