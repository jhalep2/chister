package com.example.chister

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class IngresarChiste : AppCompatActivity() {

    lateinit var mFirebaseDatabase:FirebaseDatabase;
    lateinit var referenciaBD:DatabaseReference;
    lateinit var editTextTitulo : EditText
    lateinit var editTextChiste : EditText
    lateinit var editTextPersonajes : EditText
    lateinit var guardarButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ingresar_chiste)

        editTextTitulo = findViewById(R.id.edit_text_titulo)
        editTextChiste = findViewById(R.id.edit_text_chiste)
        editTextPersonajes= findViewById(R.id.edit_text_personajes)
        guardarButton = findViewById(R.id.guardar_button)

        mFirebaseDatabase = FirebaseDatabase.getInstance()
        referenciaBD = mFirebaseDatabase.getReference().child("chistes")

        guardarButton.setOnClickListener {
            guardarChiste()
            finish()
        }
    }

    fun guardarChiste(){
        val chisteId = referenciaBD.push()?.key ?:return
        val chiste = Chiste(chisteId,editTextTitulo.text.toString(),editTextChiste.text.toString(),editTextPersonajes.text.toString())
        referenciaBD.child(chisteId).setValue(chiste).addOnCompleteListener{
            Toast.makeText(applicationContext,"Chiste guardado exitosamente",Toast.LENGTH_SHORT).show()
        }
        editTextChiste.setText("")
        editTextPersonajes.setText("")
    }

}
